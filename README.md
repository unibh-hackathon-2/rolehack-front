# WebSite FrontEnd

`Esse projeto deve ser executado em conjunto com o hybrid-app contido neste` [repositório](https://gitlab.com/unibh-hackathon-2/rolehack-agent-hybrid-app).

A solução desenvolvida e versionada nesse repositório resolve uma demanda da COP-BH (Centro de Operações de Belo Horizonte), o qual possuia a necessidade de visualizar em tempo real a localização dos agentes públicos da prefeitura de Belo Horizonte no telão de informações do COP-BH.

O projeto foi idealizado respeitando seus requisitos básicos e utilizando tecnologias de desenvolvimento inovadoras.

Este projeto foi desenvolvido utilizando as seguintes tecnologias:

- BHMap
- OpenLayers 2
- Angular 5
- HTML / CSS
- Bootstrap 4
- Fontawesome 5
- REALTIME angular-firebase (BaaS)

![rolehack](/uploads/8b7571916ec23f6bd4853f31e3fe6d96/rolehack.gif)

## Como rodar o projeto?

Primeiro passo, após o ```git clone``` verifique que você está na branch ```master```.

Se não estiver, mude para ela usando:
```git checkout master```


 Já na master, rodar: ```npm install``` e logo após: ```ng serve```

Basta acessar `http://localhost:4200`.
