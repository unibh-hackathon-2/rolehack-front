import { Component, OnInit } from "@angular/core";

declare var OpenLayers:any; 
declare var BHMap:any; 
declare var proj4:any;

import {
  AngularFireDatabase
} from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: "app-mapa-agentes",
  templateUrl: "./mapa-agentes.component.html",
  styleUrls: ["./mapa-agentes.component.css"]
})
export class MapaAgentesComponent implements OnInit {
  agentes: Observable<any[]>;
  map: any = null;
  public markers: any;
  primeiravez = false;

  public filter = [true, true, true, true, true, true];
  icons = [];

  constructor(private db: AngularFireDatabase) {
    db.list("agentes")
      .valueChanges()
      .subscribe(result => {
        this.updateModel(result);
        this.primeiravez = true;
      });
  }

  updateModel(json) {
    this.agentes = json;
    this.updateMapa();
  }

  updateFilters() {
    this.updateMap(this.map, this.agentes);
  }

  updateMap(map, agentes) {
    const src = new OpenLayers.Projection("EPSG:4326");
    // Converte do padrão do Google Maps (4326) para o BHMaps (31893)
    const firstProjection = "+proj=longlat +datum=WGS84 +no_defs";
    const secondProjection =
      "+proj=utm +zone=23 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs";
    let marker;
    this.markers.clearMarkers();
    const elements = document.getElementsByClassName("olPopup");
    while (elements.length > 0) {
      elements[0].parentNode.removeChild(elements[0]);
    }
    for (let key in agentes) {
      if (this.filter[agentes[key].tipo]) {
        let position = new OpenLayers.LonLat(
          proj4(firstProjection, secondProjection, [
            agentes[key].lon,
            agentes[key].lat
          ])
        );
        marker = new OpenLayers.Marker(
          position,
          this.icons[agentes[key].tipo].clone()
        );

        this.markers.addMarker(marker);

        let popup;

        marker.events.register("mouseover", marker, evt => {
          popup = new OpenLayers.Popup(
            "Popup",
            position,
            null,
            `<div class="info-agente">
                    <h2>` +
              agentes[key].nome +
              `</h2>
                    <ul>
                        <li><i class="far fa-address-book"></i> ` +
              agentes[key].matricula +
              `</li>
                        <li><i class="fas fa-map-marker-alt"></i> Av perimetral, 2370 - Vila santa, BH/MG</li>
                        <li><i class="fas fa-phone"></i> ` +
              agentes[key].telefone +
              `</li>
                    <ul>
                </div>`,
            null,
            false
          );
          this.map.addPopup(popup);
        });
        marker.events.register("mouseout", marker, function(evt) {
          popup.destroy();
        });
      }
    }
    this.map.addLayer(this.markers);
  }

  updateMapa() {
    if (!this.primeiravez) {
      BHMap.startMap("map");
      let main = this;
      BHMap.afterload = function() {
        main.map = BHMap.Map;
        const size = new OpenLayers.Size(21, 25);
        const offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);

        main.icons[0] = new OpenLayers.Icon(
          "assets/img/icons/road.png",
          size,
          offset
        );
        main.icons[1] = new OpenLayers.Icon(
          "assets/img/icons/bullhorn.png",
          size,
          offset
        );
        main.icons[2] = new OpenLayers.Icon(
          "assets/img/icons/user-shield.png",
          size,
          offset
        );
        main.icons[3] = new OpenLayers.Icon(
          "assets/img/icons/trash.png",
          size,
          offset
        );
        main.icons[4] = new OpenLayers.Icon(
          "assets/img/icons/ambulance.png",
          size,
          offset
        );
        main.icons[5] = new OpenLayers.Icon(
          "assets/img/icons/clipboard.png",
          size,
          offset
        );
        main.markers = new OpenLayers.Layer.Markers("Markers");
        main.updateMap(main.map, main.agentes);

        main.map.zoomTo(1.8);
      };
    } else {
      this.updateMap(this.map, this.agentes);
    }
  }

  ngOnInit() {}
}
