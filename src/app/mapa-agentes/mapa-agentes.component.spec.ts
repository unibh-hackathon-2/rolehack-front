import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapaAgentesComponent } from './mapa-agentes.component';

describe('MapaAgentesComponent', () => {
  let component: MapaAgentesComponent;
  let fixture: ComponentFixture<MapaAgentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapaAgentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapaAgentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
